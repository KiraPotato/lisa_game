﻿using TMPro;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ScoreTextView : MonoBehaviour
{
    [Inject] private GameSettings _settings = default;

    private TextMeshProUGUI _scoreText;

    public void UpdateScore(int score)
    {
        if (_scoreText == null)
            _scoreText = GetComponent<TextMeshProUGUI>();

        _scoreText.text = string.Format(_settings.ScoreTextFormat, score);
    }
}
