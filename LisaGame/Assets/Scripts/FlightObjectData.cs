﻿using UnityEngine;

public struct FlightObjectData
{
    public bool ChangeMoveDirection { get; set; }
    public Vector3 Position { get; set; }
    public Vector3 Direction { get; set; }
    public Vector3 Destination { get; set; }

    public WorldEdge LastSelectedWorldEdge { get; set; }
    public float LastChangeDirectionTime { get; set; }
    public float MoveSpeed { get; set; }
}