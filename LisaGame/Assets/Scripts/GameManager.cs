﻿using UnityEngine;

public class GameManager
{
    private readonly ObjectHandler _objectHander;
    private readonly MovementManager _movementManager;
    private readonly ScoreTextView _scoreTextView;

    private int _currentScore = 0;

    public GameManager(ObjectHandler objectHander, MovementManager movementManager, ScoreTextView scoreTextView, ObjectClickHandler objectClickHandler)
    {
        _objectHander = objectHander;
        _movementManager = movementManager;
        _scoreTextView = scoreTextView;

        objectClickHandler.OnObjectClicked -= OnObjectClicked;
        objectClickHandler.OnObjectClicked += OnObjectClicked;

        _objectHander.OnSpawnComplete -= OnObjectsSpawned;
        _objectHander.OnSpawnComplete += OnObjectsSpawned;

        _scoreTextView.UpdateScore(_currentScore);
        _objectHander.SpawnObjects();

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private void OnObjectClicked(int objectIndex)
    {
        _currentScore++;

        _scoreTextView.UpdateScore(_currentScore);
    }

    private void OnObjectsSpawned()
    {
        _objectHander.OnSpawnComplete -= OnObjectsSpawned;
        _movementManager.RunBehavior = true;
    }
}
