#if UNITY_EDITOR
using UnityEngine;
using Zenject;

public class ClickRangeGizmo : MonoBehaviour
{
    [Inject] private GameSettings _settings = default;
    [Inject] private GameModel _model = default;

    private void OnDrawGizmos()
    {
        if (_settings != null && _model != null)
        {
            Gizmos.color = Color.red;

            for (int i = 0; i < _settings.ObjectsCount; i++)
            {
                if (_model.TryGetObjectData(i, out var data))
                {
                    Gizmos.DrawWireSphere(data.Position, _settings.ClickDistance);
                }
            }
        }
    }
}
#endif