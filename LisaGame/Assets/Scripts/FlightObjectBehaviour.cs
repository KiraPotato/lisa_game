using UnityEngine;

public class FlightObjectBehaviour : MonoBehaviour
{
    private GameModel _model;
    private int _objectIndex;

    public void Initialize(int objectIndex, GameModel model)
    {
        _model = model;
        _objectIndex = objectIndex;
    }

    private void Update()
    {
        if (_model.TryGetObjectData(_objectIndex, out FlightObjectData data))
        {
            //transform.up = data.Direction;
            transform.position = data.Position;

#if UNITY_EDITOR
            Debug.DrawLine(data.Position, data.Position + data.Direction.normalized);
#endif
        }
    }
}
