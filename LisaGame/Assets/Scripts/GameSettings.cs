﻿using UnityEngine;

[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    public string ScoreTextFormat => _scoreTextFormat;
    public int ObjectsCount => _objectsCount;
    public string ObjectPrefabName => _objectPrefabName;
    public int ScreenEdgePixelBuffer => _screenEdgePixelBuffer;
    public float ClickDistance => _clickDistance;

    public float ChangeToRandomDirectionChance => _changeToRandomDirectionChance;
    public float ChangeToRandomDirectionCooldown => Random.Range(_chnageToRandomDirectionCooldownMinMax.x, _chnageToRandomDirectionCooldownMinMax.y);

    [SerializeField] private Vector2 _minMaxMoveSpeed = default;
    [SerializeField] private int _objectsCount = default;
    [SerializeField] private string _objectPrefabName = default;
    [SerializeField] private int _screenEdgePixelBuffer = default;
    [SerializeField] private float _clickDistance = default;
    [SerializeField] private string _scoreTextFormat = "Score: {0}";
    [Space]
    [SerializeField, Range(0f, 100f)] private float _changeToRandomDirectionChance = 0f;
    [SerializeField] private Vector2 _chnageToRandomDirectionCooldownMinMax = default;

    public float FetchRandomMoveSpeed() => _minMaxMoveSpeed.GetRandom();
}
