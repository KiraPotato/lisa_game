﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ObjectHandler
{
    public event OnSpawnCompleteDelegate OnSpawnComplete;

    private readonly GameModel _model;
    private readonly GameSettings _settings;
    private readonly Camera _mainCamera;
    private readonly FlightObjectBehaviour[] _objects;

    public ObjectHandler(GameModel model, GameSettings settings, Camera mainCamera)
    {
        _model = model;
        _settings = settings;
        _mainCamera = mainCamera;

        _objects = new FlightObjectBehaviour[settings.ObjectsCount];
    }

    public void SpawnObjects()
    {
        Addressables.LoadAssetAsync<GameObject>(_settings.ObjectPrefabName).Completed += OnPrefabLoaded;

        void OnPrefabLoaded(AsyncOperationHandle<GameObject> obj)
        {
            FlightObjectBehaviour prefab = obj.Result.GetComponent<FlightObjectBehaviour>();

            for (int currIndex = 0; currIndex < _settings.ObjectsCount; currIndex++)
            {
                var data = new FlightObjectData();
                int pixelBuffer = _settings.ScreenEdgePixelBuffer;

                data.Position = _mainCamera.GetRandomScreenEdge(pixelBuffer, out WorldEdge selectedWorldEdge);
                data.Destination = _mainCamera.GetRandomScreenEdge(pixelBuffer, out WorldEdge selectedDestiantionEdge, selectedWorldEdge);
                data.Direction = (data.Destination - data.Position).normalized;
                data.MoveSpeed = _settings.FetchRandomMoveSpeed();
                data.LastSelectedWorldEdge = selectedDestiantionEdge;

                _model.SetObjectData(currIndex, data);

                FlightObjectBehaviour behaviour = Object.Instantiate(prefab, data.Position, Quaternion.Euler(data.Direction));
                behaviour.Initialize(currIndex, _model);
                _objects[currIndex] = behaviour;
            }

            OnSpawnComplete?.Invoke();
        }
    }

}
