﻿public class GameModel
{
    private FlightObjectData[] _flyingObjects;

    public GameModel(GameSettings settings)
    {
        _flyingObjects = new FlightObjectData[settings.ObjectsCount];
    }

    public void SetObjectData(int index, FlightObjectData data)
    {
        if (_flyingObjects.IsIndexInRange(index) == false)
            return;

        _flyingObjects[index] = data;
    }

    public bool TryGetObjectData(int index, out FlightObjectData data)
    {
        if (_flyingObjects.IsIndexInRange(index) == false)
        {
            data = default;
            return false;
        }

        data = _flyingObjects[index];
        return true;
    }
}
