﻿using UnityEngine;
using Zenject;

public static class Utilities
{
    public static void BindSingleNonLazy<TObjectType>(this DiContainer container, TObjectType instance = default)
    {
        if (instance == null)
        {
            container.BindInterfacesAndSelfTo<TObjectType>().AsSingle().NonLazy();
        }
        else
        {
            container.BindInterfacesAndSelfTo<TObjectType>().FromInstance(instance).AsSingle().NonLazy();
        }
    }

    public static float GetRandom(this Vector2 vector)
        => Random.Range(vector.x, vector.y);

    public static bool IsIndexInRange<TElementType>(this TElementType[] array, int index)
        => index >= 0 && index < array.Length;

    public static Vector3 GetRandomScreenEdge(this Camera camera, int pixelBuffer, out WorldEdge selectedEdge, WorldEdge? excludeEdge = null)
    {
        selectedEdge = (WorldEdge)Random.Range(0, 4);

        if (excludeEdge != null)
        {
            while (true)
            {
                if (selectedEdge != excludeEdge)
                    break;

                selectedEdge = (WorldEdge)Random.Range(0, 4);
            }
        }

        Vector2 screenPos = Vector2.zero;

        switch (selectedEdge)
        {
            case WorldEdge.Top:
                screenPos.x = Random.Range(0f, camera.pixelWidth);
                screenPos.y = camera.pixelHeight + pixelBuffer;
                break;

            case WorldEdge.Bottom:
                screenPos.x = Random.Range(0f, camera.pixelWidth);
                screenPos.y = -pixelBuffer;
                break;

            case WorldEdge.Left:
                screenPos.x = -pixelBuffer;
                screenPos.y = Random.Range(0f, camera.pixelHeight);
                break;

            case WorldEdge.Right:
                screenPos.x = camera.pixelWidth + pixelBuffer;
                screenPos.y = Random.Range(0f, camera.pixelHeight);
                break;
        }

        return camera.ScreenToWorldPoint(screenPos);
    }
}

public enum WorldEdge
{
    Top = 0,
    Bottom = 1,
    Left = 2,
    Right = 3
}
