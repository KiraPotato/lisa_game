using UnityEngine;
using Zenject;

public class GameSceneInstaller : MonoInstaller
{
    [SerializeField] private ScoreTextView _scoreTextView = default;
    [SerializeField] private GameSettings _gameSettings = default;
    [SerializeField] private Camera _gameCamera = default;

    public override void InstallBindings()
    {
        Container.BindSingleNonLazy(_scoreTextView);
        Container.BindSingleNonLazy(_gameCamera);
        Container.BindSingleNonLazy(_gameSettings);

        Container.BindSingleNonLazy<GameModel>();
        Container.BindSingleNonLazy<ObjectHandler>();
        Container.BindSingleNonLazy<MovementManager>();
        Container.BindSingleNonLazy<ObjectClickHandler>();
        Container.BindSingleNonLazy<GameManager>();
    }
}