﻿using UnityEngine;
using Zenject;

public class MovementManager : ITickable
{
    private const float MIN_DISTANCE_FOR_DIR_UPDATE = .01f;

    public bool RunBehavior { get; set; }

    private GameSettings _settings;
    private Camera _camera;
    private GameModel _model;

    public MovementManager(GameSettings settings, Camera camera, GameModel model)
    {
        _settings = settings;
        _camera = camera;
        _model = model;
    }

    public void Tick()
    {
        if (RunBehavior == false)
            return;

        for (int i = 0; i < _settings.ObjectsCount; i++)
        {
            MoveObject(i, Time.deltaTime, Time.time);
        }
    }

    private void MoveObject(int index, float deltaTime, float currentTime)
    {
        if (_model.TryGetObjectData(index, out FlightObjectData data) == false)
            return;

        float speed = data.MoveSpeed * deltaTime;

        data.Position = Vector3.MoveTowards(data.Position, data.Destination, speed);

        if (ShouldRandomDirChange(ref data, currentTime)
            ||data.ChangeMoveDirection
            || Vector3.Distance(data.Position, data.Destination) < MIN_DISTANCE_FOR_DIR_UPDATE)
        {
            data.Destination = _camera.GetRandomScreenEdge(_settings.ScreenEdgePixelBuffer, out WorldEdge selectedEdge, data.LastSelectedWorldEdge);
            data.LastSelectedWorldEdge = selectedEdge;
            data.Direction = (data.Destination - data.Position).normalized;
            data.MoveSpeed = _settings.FetchRandomMoveSpeed();
            data.ChangeMoveDirection = false;
        }

        _model.SetObjectData(index, data);
    }

    private bool ShouldRandomDirChange(ref FlightObjectData data, float currentTime)
    {
        float cooldown = _settings.ChangeToRandomDirectionCooldown;
        float lastChange = currentTime - data.LastChangeDirectionTime;

        if (lastChange > cooldown)
        {
            data.LastChangeDirectionTime = currentTime;
            return Random.Range(0f, 100f) > _settings.ChangeToRandomDirectionChance;
        }

        return false;
    }
}