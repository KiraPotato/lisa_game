﻿using UnityEngine;
using Zenject;

public class ObjectClickHandler : ITickable
{
    public event OnObjectClickedDelegate OnObjectClicked;

    private GameModel _model;
    private GameSettings _settings;
    private Camera _camera;

    public ObjectClickHandler(GameModel model, GameSettings settings, Camera camera)
    {
        _model = model;
        _settings = settings;
        _camera = camera;
    }

    public void Tick()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) == false)
            return;

        for (int index = 0; index < _settings.ObjectsCount; index++)
        {
            if (_model.TryGetObjectData(index, out FlightObjectData data))
            {
                if (WasObjectClicked(data.Position))
                {
                    OnObjectClicked?.Invoke(index);

                    data.ChangeMoveDirection = true;

                    _model.SetObjectData(index, data);
                }
            }
        }
    }

    private bool WasObjectClicked(Vector3 worldPosition)
    {
        Vector3 clickPos = _camera.ScreenToWorldPoint(Input.mousePosition);

        return Vector3.Distance(worldPosition, clickPos) < _settings.ClickDistance;
    }
}